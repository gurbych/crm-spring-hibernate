package com.springdemo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.springdemo.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO {
	
	// inject the session factory
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	// @Transactional // moved to Service Layer
	public List<Customer> getCustomers() {
		
		// get the current hibernate session
		Session session = sessionFactory.getCurrentSession();
		
		// create a query
		Query<Customer> query = session.createQuery("from Customer order by lastName",
													Customer.class);
		
		// execute query and get result list
		List<Customer> customers = query.getResultList();
		
		// return the results
		return customers;
	}

	@Override
	public void saveCustomer(Customer customer) {
		// get current hibernate session
		Session session = sessionFactory.getCurrentSession();
		
		// save the customer
		session.saveOrUpdate(customer);
	}

	@Override
	public Customer getCustomer(int id) {
		// get the current hibernate session
		Session session = sessionFactory.getCurrentSession();
		
		// read the object from the database using primary key
		Customer customer = session.get(Customer.class, id);
		
		// return it
		return customer;
	}

	@Override
	public void deleteCustomer(int id) {
		// get the current hibernate session
		Session session = sessionFactory.getCurrentSession();
		
		// delete the object by primary key
		Query query = session.createQuery(
				"delete from Customer where id=:customerId");
		query.setParameter("customerId", id);
		
		// execute the query
		query.executeUpdate();
	}

}
